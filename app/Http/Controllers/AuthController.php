<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    // NOTE login
    // POST /api/auth/login
    public function login(Request $request)
    {
        $rules = [
            'username'  => 'required',
            'password'  => 'required|min:8'
        ];

        $messages = [
            'username.required' => 'Username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'password.min'      => 'Password harus mengandung setidaknya 8 karakter',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [];

            foreach ($validator->errors()->messages() as $row => $key) {
                $data[] = $key;
            }

            return apiResponse('Data tidak lengkap', 400, $data);
        }

        $user = DB::table('users')->where('username', $request->username)->first();

        if (!$user) {
            return apiResponse('Akun tidak ditemukan', 400);
        }

        $data = [
            'email'     => $user->email,
            'password'  => $request->password,
        ];

        if (!Auth::attempt($data)) {
            return apiResponse('Password tidak cocok', 400);
        }

        $token = Auth::user()->createToken('API Token')->accessToken;

        $identifier = DB::table('users')->where('id', Auth::user()->id)->first();

        $user = [
            'id'        => $identifier->id,
            'name'      => $identifier->name,
            'username'  => $identifier->username,
            'email'     => $identifier->email,
            // 'role'      => Auth::user()->getRoleNames()[0],
        ];

        $data   = [
            'token'     => $token,
            'user'      => $user,
        ];

        return apiResponse('Login berhasil', 200, $data);
    }
    // NOTE logout
    // POST api/auth/logout
    public function logout()
    {
        if (auth('api')->check()) {
            auth('api')->user()->token()->revoke();
        }

        return apiResponse('Logout berhasil', 200);
    }
}
