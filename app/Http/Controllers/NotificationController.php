<?php

namespace App\Http\Controllers;

use App\Events\NotificationRetreived;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    // NOTE ambil notif
    // GET /api/notification
    /* -------------------------------------------------------------------------- */
    /*                                 AMBIL NOTIF                                */
    /* -------------------------------------------------------------------------- */
    public function index()
    {
        $data = [[
            'a' => 'a',
            'b' => 'b',
        ], [
            'c' => 'c',
            'd' => 'd',
        ]];

        return apiResponse('Data found', 200, $data);
    }

    // NOTE kirim notif
    // POST /api/notification
    /* -------------------------------------------------------------------------- */
    /*                                 KIRIM NOTIF                                */
    /* -------------------------------------------------------------------------- */
    public function store(Request $request)
    {
        $newNotification = [
            $request->sender    => $request->sender,
            $request->message   => $request->message,
        ];

        return apiResponse('Ok', 200);
    }
}
