<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    private $valid = true;

    function __construct()
    {
        if (auth('api')->user() && auth('api')->user()->getRoleNames()[0] !== 'Super Admin') {
            $this->valid =  false;
        }
    }
    // NOTE list user
    // GET /api/users
    public function index()
    {
        if (!$this->valid) {
            return apiResponse('Anda tidak memiliki hak untuk mengakses fitur ini', 403);
        }

        $data = DB::table('users')->where('username', '<>', 'supra')->get();

        foreach ($data as $row) {
            $role_id = DB::table('model_has_roles')
                ->where('model_id', $row->id)
                ->first()
                ->role_id;

            $role = DB::table('roles')->where('id', $role_id)->first()->name;

            $row->id            = $row->id;
            $row->name          = $row->name;
            $row->username      = $row->username;
            $row->email         = $row->email;
            $row->created_at    = $row->created_at;
            $row->role          = $role;
            $row->role_id       = $role_id;
        }

        return apiResponse('Data ditemukan', 200, $data);
    }
    // NOTE hapus user
    // DELETE /api/user/{id}
    public function destroy($id)
    {
        if (!$this->valid) {
            return apiResponse('Anda tidak memiliki hak untuk mengakses fitur ini', 403);
        }

        try {
            User::where('id', $id)->delete();

            return apiResponse('User berhasil dihapus', 200);
        } catch (Exception $e) {
            if (env('APP_ENV') == 'local') {
                dd($e);
            }

            return apiResponse('Terjadi kesalahan', 400, $e->getMessage());
        }
    }
    // NOTE ambil user
    // GET /api/user/{id}
    public function show($id)
    {
        if (!$this->valid) {
            return apiResponse('Anda tidak memiliki hak untuk mengakses fitur ini', 403);
        }

        $user = DB::table('users')->where('id', $id)->first();

        if (!$user) {
            return apiResponse('Data tidak ditemukan', 404);
        }

        $role_id = DB::table('model_has_roles')
            ->where('model_id', $id)
            ->first()
            ->role_id;

        $role = DB::table('roles')->where('id', $role_id)->first()->name;

        $data = [
            'id'            => $user->id,
            'name'          => $user->name,
            'username'      => $user->username,
            'email'         => $user->email,
            'created_at'    => $user->created_at,
            'role'          => $role,
            'role_id'       => $role_id,
        ];

        return apiResponse('Data ditemukan', 200, $data);
    }
    // NOTE insert user
    // POST /api/user
    public function store(Request $request)
    {
        if (!$this->valid) {
            return apiResponse('Anda tidak memiliki hak untuk mengakses fitur ini', 403);
        }

        try {
            $rules = [
                'name'      => 'required',
                'email'     => 'required|unique:users,email',
                'password'  => 'required|min:8',
                'username'  => 'required|unique:users,username',
                'role_id'   => 'required',
            ];

            $messages = [
                'name.required'         => 'Nama wajib diisi',
                'email.required'        => 'E-mail wajib diisi',
                'email.unique'          => 'E-mail sudah terdaftar',
                'password.required'     => 'Password wajib diisi',
                'password.min'          => 'Password harus mengandung minimal 8 karakter',
                'username.required'     => 'Username wajib diisi',
                'username.unique'       => 'Username sudah terdaftar',
                'role_id.required'      => 'Role wajib diisi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                $data = [];

                foreach ($validator->errors()->messages() as $row => $key) {
                    $data[] = $key;
                }

                return apiResponse('Data tidak lengkap', 400, $data);
            }

            $role = Role::where('id', $request->role_id)->first();

            DB::transaction(function () use ($request, $role) {
                $data   = [
                    'email'     => $request->email,
                    'name'      => $request->name,
                    'password'  => Hash::make($request->password),
                    'username'  => $request->username,
                ];

                $user   = User::create($data);

                $user->syncRoles($role);

                DB::table('user_details')->insert([
                    'created_at'    => date('Y-m-d H:i:s'),
                    'user_id'       => $user->id
                ]);
            });

            return apiResponse('User berhasil ditambahkan', 201);
        } catch (Exception $e) {
            if (env('APP_ENV') == 'local') {
                dd($e);
            }

            return apiResponse('Terjadi kesalahan', 400, $e->getMessage());
        }
    }
    // NOTE update user
    // POST /api/user/{id}
    public function update(Request $request, $id)
    {
        if (!$this->valid) {
            return apiResponse('Anda tidak memiliki hak untuk mengakses fitur ini', 403);
        }

        try {
            if ($request->has('password')) {
                $rules = [
                    'name'      => 'required',
                    'email'     => 'required|unique:users,email,' . $id,
                    'password'  => 'required|min:8',
                    'username'  => 'required|unique:users,username,' . $id,
                ];

                $messages = [
                    'name.required'     => 'Nama wajib diisi',
                    'email.required'    => 'E-mail wajib diisi',
                    'email.unique'      => 'E-mail sudah terdaftar',
                    'password.required' => 'Password wajib diisi',
                    'password.min'      => 'Password harus mengandung minimal 8 karakter',
                    'username.required' => 'Username wajib diisi',
                    'username.unique'   => 'Username sudah terdaftar',
                ];
            } else {
                $rules = [
                    'name'      => 'required',
                    'email'     => 'required|unique:users,email,' . $id,
                    'username'  => 'required|unique:users,username,' . $id,
                ];

                $messages = [
                    'name.required'     => 'Nama wajib diisi',
                    'email.required'    => 'E-mail wajib diisi',
                    'email.unique'      => 'E-mail sudah terdaftar',
                    'username.required' => 'Username wajib diisi',
                    'username.unique'   => 'Username sudah terdaftar',
                ];
            }

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                $data = [];

                foreach ($validator->errors()->messages() as $row => $key) {
                    $data[] = $key;
                }

                return apiResponse('Data tidak lengkap', 400, $data);
            }

            if ($id < 4) {
                $role_id = DB::table('model_has_roles')
                    ->where('model_id', $id)
                    ->first()
                    ->role_id;

                $role = Role::where('id', $role_id)->first();
            } else {
                $role = Role::where('id', $request->role_id)->first();
            }

            DB::transaction(function () use ($request, $role, $id, $signature) {
                $user = User::where('id', $id)->first();

                $data   = [
                    'email'     => $request->email,
                    'name'      => $request->name,
                    'password'  => $request->has('password') ? Hash::make($request->password) : $user->password,
                    'username'  => $request->username,
                ];

                User::where('id', $id)->update($data);

                $old_role = $user->getRoleNames()[0];

                $old_role = Role::where('name', $old_role)->first();

                $user->removeRole($old_role);
                $user->syncRoles($role);

                DB::table('user_details')->insert([
                    'updated_at'    => date('Y-m-d H:i:s'),
                    'user_id'       => $user->id
                ]);
            });

            return apiResponse('User berhasil diperbaharui', 200);
        } catch (Exception $e) {
            if (env('APP_ENV') == 'local') {
                dd($e);
            }

            return apiResponse('Terjadi kesalahan', 400, $e->getMessage());
        }
    }
}
