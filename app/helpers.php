<?php
// NOTE respon API
function apiResponse($message, $code, $data = null)
{
    return response()->json([
        'statuscode'    => $code,
        'message'       => $message,
        'data'          => $data
    ], $code);
}
// NOTE get user field id
function myFieldId()
{
    $identifier = DB::table('single_user_view')
        ->where('id', auth('api')->user()->id)
        ->first();

    $field = DB::table('field_view')->where('id', $identifier->field_id)->first();

    return $field ? $field->id : '';
}
// NOTE routing
function includeRouteFiles($folder)
{
    $directory = $folder;
    $handle = opendir($directory);
    $directory_list = [$directory];

    while (false !== ($filename = readdir($handle))) {
        if ($filename != '.' && $filename != '..' && is_dir($directory . $filename)) {
            array_push($directory_list, $directory . $filename . '/');
        }
    }

    foreach ($directory_list as $directory) {
        foreach (glob($directory . '*.php') as $filename) {
            require $filename;
        }
    }
}
// NOTE string to array
function stringtoArray($string)
{
    $hay    = [
        '[', ']', '"'
    ];

    $data   = str_replace($hay, '', $string);

    $data   = explode(',', $data);

    return $data;
}
