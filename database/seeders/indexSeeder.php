<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class indexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // $supra = Role::create([
        //     'name' => 'Super Admin'
        // ]);

        // $admin = Role::create([
        //     'name' => 'Admin'
        // ]);

        // $userRole = Role::create([
        //     'name' => 'User'
        // ]);

        $data   = [
            'email'     => $faker->unique()->safeEmail,
            'name'      => $faker->name,
            'password'  => Hash::make('12345678'),
            'username'  => 'admin',
        ];

        $user   = User::create($data);

        // $user->syncRoles($supra);

        DB::table('user_details')->insert([
            'created_at'    => date('Y-m-d H:i:s'),
            'user_id'       => $user->id
        ]);

        // $data   = [
        //     'email'     => $faker->unique()->safeEmail,
        //     'name'      => $faker->name,
        //     'password'  => Hash::make('12345678'),
        //     'username'  => 'admin',
        // ];

        // $user   = User::create($data);

        // $user->syncRoles($admin);

        // DB::table('user_details')->insert([
        //     'created_at'    => date('Y-m-d H:i:s'),
        //     'user_id'       => $user->id
        // ]);

        // $data   = [
        //     'email'     => $faker->unique()->safeEmail,
        //     'name'      => $faker->name,
        //     'password'  => Hash::make('12345678'),
        //     'username'  => 'user',
        // ];

        // $user   = User::create($data);

        // $user->syncRoles($userRole);

        // DB::table('user_details')->insert([
        //     'created_at'    => date('Y-m-d H:i:s'),
        //     'user_id'       => $user->id
        // ]);

        Artisan::call('passport:install');
    }
}
